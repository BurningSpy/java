import java.util.Scanner;

import beispieltaschenrechner.DivideByZeroException;
import beispieltaschenrechner.Taschenrechner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		
		try {
		
			int Zahl1 = scan.nextInt();
			
			int Zahl2 = scan.nextInt();
			
			Taschenrechner.divide(Zahl1, Zahl2);
		}
		catch (DivideByZeroException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("programm geht weiter");
		
	}

}
