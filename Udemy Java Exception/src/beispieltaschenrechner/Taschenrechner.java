package beispieltaschenrechner;

public class Taschenrechner {

	
	public static void add(double zahl1, double zahl2) {
		System.out.println("Ergebnis: " + (zahl1 + zahl2));
	}

	public static void subtract(double zahl1, double zahl2) {
		System.out.println("Ergebnis: " + (zahl1 + zahl2));
	}
	
	public static void divide(double zahl1, double zahl2) throws DivideByZeroException {
		if ( zahl2 == 0)
		{
			throw new DivideByZeroException("Dont Divide by zero");
		}else {System.out.println("Ergebnis: " + (zahl1/zahl2));}
	}
	
}
