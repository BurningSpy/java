import java.util.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		List<String> liste = new ArrayList<>();
		
		Set<String> set = new TreeSet<String>();
		Set<String> hashSet = new HashSet<>();
		Set<String> lHashSet = new LinkedHashSet<>();
		
		//ArrayListe
		liste.add("Bob");
		liste.add("Tim");
		liste.add("Jerry");
		liste.add("Frank");
		liste.add("Tim");
		
		//TreeSet
		set.add("Bob");
		set.add("Tim");
		set.add("Jerry");
		set.add("Frank");
		set.add("Tim");
		
		//Hashset
		hashSet.add("Bob");
		hashSet.add("Tim");
		hashSet.add("Jerry");
		hashSet.add("Frank");
		hashSet.add("Tim");
		
		//LinkedHashset
		lHashSet.add("Bob");
		lHashSet.add("Tim");
		lHashSet.add("Jerry");
		lHashSet.add("Frank");
		lHashSet.add("Tim");
		
		
		
		System.out.println("Liste: ");
		getElement(liste);
		System.out.println("\n Set");
		getElement(set);
		System.out.println("\n Hashset");
		getElement(hashSet);
		System.out.println("\n LinkedHashSet");
		getElement(lHashSet);
		
	}

	public static void getElement(Collection<String> collection) {
		for (String string : collection) {
			System.out.println(string);
		}
	}
}
