
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		countIterativ(1, 10);
		
		countRekursiv(5,15);
	}



//Iterative Methoden:
public static void countIterativ(int zahl, int max) {
	while (zahl <= max) {
		System.out.println("Iterativ: " + zahl);
		zahl++; 
		
		}
	}

//rekursive Methode: 
public static void countRekursiv(int zahl, int max) {
	if (zahl>max) {
		return;
	}
	System.out.println("Rekursiv: " + zahl);
	countRekursiv(zahl+1, max);
}

}