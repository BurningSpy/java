package application;
	
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;


public class Main extends Application {
	
	Rectangle2D screensize = Screen.getPrimary().getVisualBounds();
	
	@Override
	public void start(Stage primaryStage) {
		try {
			
			//Root -- Layout der Szene
			HBox root = new HBox();
			
			root.setPrefSize(70.0, 70.0);
			root.setSpacing(10.0);
			root.setAlignment(Pos.CENTER);
			root.getStyleClass().add("root");
			
			//Elemente erstellen (Button etc)
			Button youtubeButton = new Button();
			Button facebookButton = new Button();
			Button amazonButton = new Button();
			Button twitterButton = new Button();
			Button exitButton = new Button();
			
			youtubeButton.setPrefSize(64.0, 64.0);
			facebookButton.setPrefSize(64.0, 64.0);
			amazonButton.setPrefSize(64.0, 64.0);
			twitterButton.setPrefSize(64.0, 64.0);
			exitButton.setPrefSize(64.0, 64.0);
			
			youtubeButton.getStyleClass().add("youtubeButton");
			facebookButton.getStyleClass().add("facebookButton");
			amazonButton.getStyleClass().add("amazonButton");
			twitterButton.getStyleClass().add("twitterButton");
			exitButton.getStyleClass().add("exitButton");
			
			//Elemente der root hinzufügen
			root.getChildren().add(youtubeButton);
			root.getChildren().add(facebookButton);
			root.getChildren().add(amazonButton);
			root.getChildren().add(twitterButton);
			root.getChildren().add(exitButton);
			
			//Eventhandler -- Buttons benutzbar machen
			youtubeButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					getHostServices().showDocument("https://www.youtube.com");
					
				}
			});
			
			facebookButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					getHostServices().showDocument("https://www.facebook.com");
					
				}
			});
			
			amazonButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					getHostServices().showDocument("https://www.amazon.de");					
				}
			});

			twitterButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					getHostServices().showDocument("https://www.twitter.com");					
				}
			});
			
			exitButton.setOnAction(new EventHandler<ActionEvent>() {
				
				@Override
				public void handle(ActionEvent arg0) {
					System.exit(0);

					
				}
			});



			
		
			
			Scene scene = new Scene(root,350,70);
			scene.setFill(Color.TRANSPARENT);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			
			//Positionieren und transparent machen
			primaryStage.setX((screensize.getWidth() / 2)-200); //X-position
			primaryStage.setY(20.0); //X-position
			primaryStage.initStyle(StageStyle.TRANSPARENT);
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
