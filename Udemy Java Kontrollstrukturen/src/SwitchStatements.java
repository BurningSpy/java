import java.util.Scanner;

public class SwitchStatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner scan = new Scanner(System.in);
		System.out.println("Eingabe bitte: ");
		
		int Eingabe = scan.nextInt();
		
		switch (Eingabe) {
		case 0:
			System.out.println("Zahl ist 0");
			break;
		case 3:
			System.out.println("Zahl ist 3");
			break;
		case 5:
			System.out.println("Zahl ist 5");
			break;
		default: 
			System.out.println("Zahl unbekannt");
			break;
		}
		
	}

}
