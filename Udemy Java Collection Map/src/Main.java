import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class Person{
	private int id;
	private String name;
	
	public Person(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String toString() {
		return "ID " + this.id + " Name " + this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Map<Integer, String> map1 = new HashMap<>();
		
		map1.put(123, "Boobs");
		map1.put(321, "Dick");
		
		System.out.println(map1.get(321));
		
		for (Integer key : map1.keySet()) {
			System.out.println(key);
			String value = map1.get(key);
			System.out.println(value);
		}

		System.out.println("\n"+ map1.keySet());
		
		Person p1 = new Person(1, "Peter");
		Person p2 = new Person(2, "Hans");
		Person p3 = new Person(3, "Honinchen");
		Person p4 = new Person(4, "Bi");
		
		Set<Person> set1 = new HashSet<Person>();
		
		set1.add(p1);
		set1.add(p2);
		set1.add(p3);
		set1.add(p4);
		
		
	}

}
