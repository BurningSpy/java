
public abstract class Tier {

	String name;
	int gr��e;
	int gewicht;

	public Tier() {
		
	}
	
	public Tier(String name, int gr��e, int gewicht) {

		this.name = name;
		this.gr��e = gr��e;
		this.gewicht = gewicht;
	}

	public abstract void essen();
	
	public abstract void atmen();
	
	public String getName() {
		return name;
	}
}
