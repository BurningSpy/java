
public class Hund extends Tier {

	int z�hne;

	public Hund(String name, int gr��e, int gewicht, int z�hne) {
		super(name, gr��e, gewicht); // �bergibt die Konstruktorparameter an die Superklasse Tier
		this.z�hne = z�hne;
	}

	// Methoden
	public void bellen() {
		System.out.println("Hund " + this.name + " sagt wuff!");
	}

	void getData() {
		System.out.println(this.name + " ist " + this.gr��e + "cm gro�, wiegt " + this.gewicht + "kg und hat "
				+ this.z�hne + "Z�hne");
	}

	@Override
	public void essen() {
	}

	@Override
	public void atmen() {
		// TODO Auto-generated method stub
		
	}

}
