package de.anonyme.klasse;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Bisher:
		Person p1 = new Person("Wau");
		
		//Anonym: 
		
		new Person("Miau") {
			
			@Override
			public void printName() {
				// TODO Auto-generated method stub
				System.out.println("Der Name " + this.getName());
			}
			
		};
		
		Hund h1 = new Hund();
		System.out.println(h1.machLaute());
		
		Tier elefant = new Tier() {

			@Override
			public String machLaute() {
				return "Duude";
			}
		
		};
		
		Tier schwein = new Tier() {

			@Override
			public String machLaute() {
				// TODO Auto-generated method stub
				return "Maaan";
			}
		};
		
		System.out.println(schwein.machLaute() + " " + elefant.machLaute());
	}
}
