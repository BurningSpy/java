package de.lokale.klasse;

public class Mannschaft {

	//Methode der Klasse Mannschaft
	public void erstelleSportler() {
		
		class Sportler{
			String name; 
			int alter;
			
			public Sportler(String _name, int _alter) {
				name = _name;
				alter = _alter;
			}
			
			public void getSportler() {
				System.out.println("Sportler erstellt: " + this.name + " Alter: " + this.alter);
				
			}
		}
		
		Sportler sp1 = new Sportler("Fred", 24);
		Sportler sp2 = new Sportler("Wau", 24);
		Sportler sp3 = new Sportler("Miau", 23);
		
		sp1.getSportler();
		sp2.getSportler();
		sp3.getSportler();
		
		
		
	}
}
