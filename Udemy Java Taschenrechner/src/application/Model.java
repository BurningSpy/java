package application;

public class Model {
	
	public double calculate(double number1, double number2, String operation) {
		
		switch (operation) {
		case "+":
			return number1 + number2;
			
		case "-":
			return number1 - number2;
			
		case "*":
			return number1 * number2;
			
		case "/":
			if(number2 == 0) {
				System.out.println("Divide by zero error!");
				return 0;
			}
			else {return number1 / number2;}
			
		
		default:
			break;
		}
		
		return 0;
		
	}
}
