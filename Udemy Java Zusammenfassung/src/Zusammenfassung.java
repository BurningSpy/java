import java.util.*;

class Person implements Comparable<Person> {
	private String vorname;

	public Person(String vorname) {
		super();
		this.vorname = vorname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vorname == null) ? 0 : vorname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (vorname == null) {
			if (other.vorname != null)
				return false;
		} else if (!vorname.equals(other.vorname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [vorname=" + vorname + "]";
	}

	@Override
	public int compareTo(Person o) {

		int laenge1 = vorname.length();
		int laenge2 = o.vorname.length();

		if (laenge1 > laenge2) {
			return 1;										// gr��er als = 1
		} else if (laenge1 < laenge2) {						// kleiner als  = -1
			return -1;										// gleich = 0
		} else {
			return vorname.compareTo(o.vorname); 			//CompareTo vergleicht Inhalte
		}

//		return vorname.compareTo(o.vorname); // Trick: Minus for Klammer kehrt Funktionsweise um.
	}

}

public class Zusammenfassung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Strukturen

		// Liste
		List<Person> list = new ArrayList<>();
		//
		// Zusammenfassung.addElements(3, list);
		// Collections.sort(list);
		// Zusammenfassung.getElements(list);
		//
		// Set
		Set<Person> set = new TreeSet<>();

		Zusammenfassung.addElements(3, set);

		Zusammenfassung.getElements(set);

		// Map
		Map<Integer, String> map = new HashMap<>();

		map.put(1, "Miau");
		map.put(2, "Wau");

		for (Integer key : map.keySet()) {
			System.out.println(key + " " + map.get(key));
		}
	}

	public static void addElements(int anzahlPersonen, Collection<Person> col) {
		for (int i = 1; i <= anzahlPersonen; i++) {
			System.out.println("Name eingeben:");
			Scanner scanName = new Scanner(System.in);
			String eingabeName = scanName.next();

			Person person = new Person(eingabeName);
			col.add(person);

		}
	}

	public static void getElements(Collection<Person> col) {
		for (Person person : col) {
			System.out.println(person);
		}
	}
}
