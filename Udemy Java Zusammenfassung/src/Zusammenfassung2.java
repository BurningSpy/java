import java.util.*;

class VergleicheStringLaenge implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		
		int laenge1 = o1.length();
		int laenge2 = o2.length();
		
		if (laenge1 > laenge2) {
			return 1;
		} else if (laenge1 < laenge2) {
			return -1;
		} else {
			return 0;
		}
	}
	
}

class UmgekehrteAlphabetischeReihenfolge implements Comparator<String>{

	@Override
	public int compare(String arg0, String arg1) {
		return -(arg0.compareTo(arg1));
	}
	
}

public class Zusammenfassung2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		List<String> angestelltenListe = new ArrayList();
	
		angestelltenListe.add("Wau");
		angestelltenListe.add("Miau");
		angestelltenListe.add("Charlotte");
		angestelltenListe.add("Honinchen");
		
		Collections.sort(angestelltenListe, new UmgekehrteAlphabetischeReihenfolge());
		
		for (String string : angestelltenListe) {
			System.out.println(string);
		}
	}

}
