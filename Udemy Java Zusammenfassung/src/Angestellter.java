
public class Angestellter {

	private int id;
	private String name;
	private int geburtsTag;
	
	public Angestellter(int id, int geburtsTag, String name) {
		this.id = id;
		this.geburtsTag = geburtsTag;
		this.name = name;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGeburtsTag() {
		return geburtsTag;
	}
	public void setGeburtsTag(int geburtsTag) {
		this.geburtsTag = geburtsTag;
	}

	@Override
	public String toString() {
		return "Angestellter [id=" + id + ", name=" + name + ", geburtsTag=" + geburtsTag + "]";
	}
	
	
}
