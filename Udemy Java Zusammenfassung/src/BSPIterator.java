import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BSPIterator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*Iteratoren haben 3 wichtige Funktionen 
		*hasNextf()
		*next()
		*und remove()
		*/
		
		List<String> list = new LinkedList<>();
		
	
		
		list.add("miau");
		list.add("wau");
		list.add("Charlene");
		list.add("Grex");
		
		Iterator<String> it = list.iterator();
		
		while(it.hasNext()) {
		String value = it.next();
		System.out.println(value);
		
		if (value.equals("Grex")) {
			it.remove();
		}
		}
		
		for (String element : list) {
			System.out.println(element);
		}
	}

}
