import java.util.*;

class SortiereAngestelltenID implements Comparator<Angestellter> {

	@Override
	public int compare(Angestellter o1, Angestellter o2) {

		if (o1.getId() > o2.getId()) {
			return 1;
		} else if (o1.getId() < o2.getId()) {
			return -1;
		} else {
			return 0;
		}

	}

}

class SortiereAngestelltenName implements Comparator<Angestellter> {

	@Override
	public int compare(Angestellter o1, Angestellter o2) {

		return o1.getName().compareTo(o2.getName());

	}

}

class SortiereAngestelltenGeburtstag implements Comparator<Angestellter> {

	@Override
	public int compare(Angestellter o1, Angestellter o2) {
		if (o1.getGeburtsTag() > o2.getGeburtsTag()) {
			return 1;
		} else if (o1.getGeburtsTag() < o2.getGeburtsTag()) {
			return -1;
		} else {
			return 0;
		}
	}

}

public class AngestelltenListe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Angestellter> angestelltenListe = new ArrayList<Angestellter>();

		AngestelltenListe.addAngestellter(3, angestelltenListe);
		
		Scanner scanSort = new Scanner(System.in);
		System.out.println("Sortiermethode? 1. ID, 2. Name, 3. Geburtstag");
		String decision = scanSort.next();
		switch (decision) {
		
		case "ID":  
			Collections.sort(angestelltenListe, new SortiereAngestelltenID());
			break;
		
		case "Name": 
			Collections.sort(angestelltenListe, new SortiereAngestelltenName());
			break;
		case "Geburtstag":
			Collections.sort(angestelltenListe, new SortiereAngestelltenGeburtstag());
			break;
			
		default: 	System.out.println("Invalid entry. Please answer with: 'ID', 'Name' or 'Geburtstag'") ;
					System.out.println("Unsorted List: ");
					break;
			
		}
		AngestelltenListe.getAngestellter(angestelltenListe);

	}

	public static void addAngestellter(int anzahl, Collection<Angestellter> col) {
		for (int i = 1; i <= anzahl; i++) {
			Scanner scanAngestellter = new Scanner(System.in);
			System.out.println("Bitte Namen eintippen:");
			String name = scanAngestellter.next();
			System.out.println("Bitte ID eintippen:");
			int id = scanAngestellter.nextInt();
			System.out.println("Bitte Geburtstag eintippen:");
			int geburtsTag = scanAngestellter.nextInt();

			Angestellter angestellter = new Angestellter(id, geburtsTag, name);
			col.add(angestellter);
		}
	}

	public static void getAngestellter(Collection<Angestellter> col) {

		for (Angestellter angestellter : col) {
			System.out.println("Angestellter: ");
			System.out.println(angestellter);
		}
	}
}
